#!/usr/bin/env pwsh
$ErrorActionPreference = "Stop"

# Update Microsoft 365 group membership from AzureAD-synced Active Directory group membership
# Requirements: 
#   Install-Module -Name AzureAD

# No need to explain
$TenantId = "<uuid>"
# Filter for the source groups
$SourceGroupsFilter = "startswith(DisplayName, 'team-')"
# Prepend or append something to the mail nickname (and default group name) for our new Microsoft 365 groups
$DestinationGroupNamePrefix = "cloud-"
$DestinationGroupNameSuffix = ""
# If you want specific users to be the owners of these Microsoft 365 groups, add their IDs below
$groupOwnerIds = @(
    # "<uuid>",
    # "<uuid>"
)

Write-Output "Please log in with an account with sufficient permissions in the pop-up window"
Connect-AzureAD -TenantId $TenantId

$SourceGroups = Get-AzureADGroup -Filter $SourceGroupsFilter

Foreach ($sGroup in $SourceGroups) {
    Write-Output "Processing group $($sGroup.DisplayName)..."
    $DestinationGroupName = $DestinationGroupNamePrefix + $sGroup.DisplayName + $DestinationGroupNameSuffix
    $DestinationGroups = Get-AzureADGroup -Filter ("MailNickname eq '${DestinationGroupName}'") # possible XSS injection?

    if ($DestinationGroups.count -eq 0) {
        Write-Output "WARNING: Destination group $DestinationGroupName does not exist, creating..."
        New-AzureADMSGroup -DisplayName $DestinationGroupName -Description "auto synced group from $($sGroup.DisplayName)" -MailEnabled $true -MailNickname $DestinationGroupName -SecurityEnabled $true -GroupTypes "Unified"
        $DestinationGroups = Get-AzureADGroup -Filter ("MailNickname eq '${DestinationGroupName}'")
    }

    $dGroup = $DestinationGroups[0]
    

    # enforce group owners
    Foreach ($groupOwnerId in $groupOwnerIds) {
        Try {
            Add-AzureADGroupOwner -ObjectId $dGroup.ObjectId -RefObjectId $groupOwnerId
        } Catch {
            # likely that user is already the group owner, do nothing
        }
    }

    $sGroupMembers = Get-AzureADGroupMember -ObjectId $sGroup.ObjectId -All $true
    $dGroupMembers = Get-AzureADGroupMember -ObjectId $dGroup.ObjectId -All $true

    # add all members in $sGroupMembers to $dGroup which is not in $dGroupMemebers
    $addedMemberCount = 0
    Foreach ($sMember in $sGroupMembers) {
        $userFound = $false
        Foreach ($dMember in $dGroupMembers) {
            if ($dMember.ObjectId -eq $sMember.ObjectId) {
                $userFound = $true
                break
            }
        }
        if ($userFound -eq $false) {
            Write-Output "Adding missing member $($sMember.DisplayName)"
            Add-AzureADGroupMember -ObjectId $dGroup.ObjectId -RefObjectId $sMember.ObjectId
            $addedMemberCount += 1
        }
    }
    Write-Output "Added missing $addedMemberCount members"

    # delete all members in $dGroupMembers from $dGroup which is not in $sGroupMembers
    $deletedMemberCount = 0
    Foreach ($dMember in $dGroupMembers) {
        $userFound = $false
        Foreach ($sMember in $sGroupMembers) {
            if ($dMember.ObjectId -eq $sMember.ObjectId) {
                $userFound = $true
                break
            }
        }
        if ($userFound -eq $false) {
            Write-Output "Removing member $($dMember.DisplayName)"
            Remove-AzureADGroupMember -ObjectId $dGroup.ObjectId -RefObjectId $dMember.ObjectId
            $deletedMemberCount += 1
        }
    }
    Write-Output "Deleted $deletedMemberCount members"
}
